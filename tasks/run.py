from invoke import task


@task(default=True)
def run(context):
    """
    Run server
    """
    from app import create_app
    app = create_app()

    return app.run()


