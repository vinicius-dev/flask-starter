import logging
import os
import sys
from flask import Flask, Blueprint
from flask_restplus import Api
from dotenv import load_dotenv, find_dotenv
from .core.config import CONFIG_ENV
from .database import db
from flask_jwt_extended import JWTManager
from app.modules.user.resources import ns as user
from .modules.api import api
app = Flask(__name__)
load_dotenv(find_dotenv())


def validate():
    env = os.getenv("FLASK_CONFIG")
    if env is None or env not in CONFIG_ENV:
        app.logger.error("Env não encontrado ou inválido")
        sys.exit(1)


def create_app():
    validate()
    app.config.from_object(CONFIG_ENV[os.getenv("FLASK_CONFIG")])
    blueprint = Blueprint("api", __name__)
    api.init_app(blueprint)
    api.add_namespace(user, path="/")
    app.register_blueprint(blueprint)
    JWTManager(app)
    db.init_app(app)
    return app


    
