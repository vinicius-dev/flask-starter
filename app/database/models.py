# coding: utf-8
from sqlalchemy import Column, DateTime, Integer, String
from app.database import db
from datetime import datetime


class User(db.Model):
    __tablename__ = "Users"

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255))
    email = db.Column(db.String(255))
    createdAt = db.Column(db.DateTime, nullable=False, default=datetime.now)
    updatedAt = db.Column(db.DateTime, nullable=False, default=datetime.now)


    def __init__(self, name, email):
        self.name = name
        self.email = email