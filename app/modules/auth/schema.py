from marshmallow import Schema, fields


class AuthSchema(Schema):
    username = fields.Str()
    password = fields.Str()
