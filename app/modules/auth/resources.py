from flask_restplus import Resource, Namespace
from flask import request
from app.core.errorhandler import ErrorHandler
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)


authAPI = Namespace("Authentication", url_prefix="/auth")


@authAPI.route("/login")
class Authentication(Resource):

    def post(self):
        req = request.get_json()
        token = create_access_token(identity=req["username"])
        return token

    @jwt_required
    def get(self):
        try:
            raise Exception("error")
        except Exception as e:
            ErrorHandler(e).handle()

