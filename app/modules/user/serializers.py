from flask_restplus import fields
from app.modules.api import api


users = api.model("Users Here", {
    "id": fields.Integer(readOnly=True, description="The unique identifier of a blog category"),
    "name": fields.String(description="User name"),
    "email": fields.String(description="User email"),
    "createdAt": fields.DateTime(description="Created at"),
    "updatedAt": fields.DateTime(description="Updated at"),
})