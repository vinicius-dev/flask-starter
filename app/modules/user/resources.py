from flask import request
from flask_restplus import Resource, Namespace, fields, marshal_with
from app.database.models import User
from .serializers import users
from .business import create_user

ns = Namespace("user", url_prefix="/user", description="user")

@ns.route("users")
class Users(Resource):
    """
    Users endpoints
    """

    @ns.marshal_with(users)
    def get(self):
        """
        Get all Users
        """
        user = User.query.all() or []
        return user


    def post(self):
        """
        Create user
        """
        create_user(request.json)
        return None, 201

    


