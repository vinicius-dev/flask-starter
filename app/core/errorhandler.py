from requests import RequestException
from marshmallow import ValidationError
from flask_restplus import abort


class ErrorHandler(object):
    def __init__(self, error):
        self.__error = error

    def handle(self):
        errors = {
            RequestException: self.handleRequestException,
            ValidationError: self.handleValidationError,
        }
        for errType, errHandler in errors.items():
            if isinstance(self.__error, errType):
                return errHandler()
        return self.defaultHandler()

    @property
    def error(self):
        return self.__error

    def handleRequestException(self):
        abort(400, error=str(self.error))

    def handleValidationError(self):
        abort(400, error=str(self.error))

    def defaultHandler(self):
        abort(500, error="Ocorreu um erro interno no servidor.")









