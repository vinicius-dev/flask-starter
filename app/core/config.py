import os


class BaseConfig(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = "SECRET"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = os.getenv("DATABASE_URI")


class DevelopmentConfig(BaseConfig):
    DEBUG = True


class TestConfig(BaseConfig):
    DEBUG = True
    TESTING = True
    SECRET_KEY = os.getenv("SECRET_KEY", default=BaseConfig.SECRET_KEY)


class ProductionConfig(BaseConfig):
    SECRET_KEY = os.getenv("SECRET_KEY")


CONFIG_ENV = dict(
    dev=DevelopmentConfig,
    test=TestConfig,
    prod=ProductionConfig,
)